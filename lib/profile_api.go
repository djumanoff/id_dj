package lib

// User profile API, get profile, update profile
type ProfileAPI interface {
	// Returns user profile in user struct
	GetProfile(userID int64) (*User, error)

	// Updates user profile (first name, last name, address)
	UpdateProfile(userID int64, userInfo *ProfileUpdate) (*User, error)
}

type ProtectedProfileAPI interface {
	// Returns user profile in user struct
	GetProfile(userID int64, accessToken string) (*User, error)

	// Updates user profile (first name, last name, address)
	UpdateProfile(userID int64, userInfo *ProfileUpdate, accessToken string) (*User, error)
}

type profileApi struct {
	userRepo UserRepository
}

type ProfileConfig struct {
	UserRepo UserRepository
}

func NewProfileAPI(cfg ProfileConfig) ProfileAPI {
	return &profileApi{
		userRepo: cfg.UserRepo,
	}
}

func (api *profileApi) GetProfile(userID int64) (*User, error) {
	return api.userRepo.Read(userID)
}

func (api *profileApi) UpdateProfile(userID int64, userInfo *ProfileUpdate) (*User, error) {
	user, err := api.userRepo.Read(userID)
	if err != nil {
		return nil, err
	}
	userUpdate := &UserUpdate{
		ProfileUpdate: userInfo,
	}
	if err := api.userRepo.Update(userID, userUpdate); err != nil {
		return nil, err
	}
	user.FirstName = *userUpdate.FirstName
	user.LastName = *userUpdate.LastName
	user.Address = *userUpdate.Address

	return user, nil
}
