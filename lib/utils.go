package lib

import (
	"golang.org/x/crypto/bcrypt"
	"regexp"
)

var (
	emailRegExp = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
)

func validateEmail(email string) error {
	if !emailRegExp.MatchString(email) {
		return ErrAuthApiInvalidEmail
	}
	return nil
}

func hashPassword(password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", ErrAuthApiPwdEncrypt
	}
	return string(hash), nil
}

func comparePasswords(hashed, purePwd string) error {
	if err := bcrypt.CompareHashAndPassword([]byte(hashed), []byte(purePwd)); err != nil {
		return ErrAuthApiInvalidCredentials
	}
	return nil
}
