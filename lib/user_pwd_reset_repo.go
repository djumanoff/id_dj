package lib

import "time"

// User password reset data
type UserPwdReset struct {
	UserID int64
	Email string
	CreatedAt time.Time
	ValidUntil time.Time
}

// Interface that describes user password resets repository capabilities, needs to be implemented by database provider
type UserPwdResetRepository interface {
	// Returns password reset token by user id
	Read(userID int64) (string, error)

	// Creates record of password reset attempt and returns reset token or error if failed
	Create(data *UserPwdReset) (string, error)

	// Deletes password reset record
	Delete(userID int64) error

	// TODO: Needs to add expiration mechanics if database provider does not support TTL functionality
}
