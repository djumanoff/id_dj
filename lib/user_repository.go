package lib

// Interface that describes user repository capabilities, needs to be implemented by database provider
type UserRepository interface {
	Create(user *User) (int64, error)

	Update(userID int64, userUpdate *UserUpdate) error

	Read(userID int64) (*User, error)

	ReadByEmail(email string) (*User, error)

	Delete(userID int64) error
}
