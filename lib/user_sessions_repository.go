package lib

import "time"

// Session of the user
type UserSession struct {
	UserID int64
	CreatedAt time.Time
	ValidUntil time.Time
}

// Interface that describes user sessions repository capabilities, needs to be implemented by database provider
type UserSessionsRepository interface {
	// Creates user session and returns access token or error
	Create(userSession *UserSession) (string, error)

	// Returns user session data by access token or error if not found
	Read(token string) (*UserSession, error)

	// Deletes user session by access token
	Delete(token string) error

	// TODO: Needs to add expiration mechanics if database provider does not support TTL functionality
}
