package lib

import (
	"errors"
	"bitbucket.org/djumanoff/id_dj/mailer"
	"strconv"
	"fmt"
)

// Authorization API interface
type AuthAPI interface {
	// Signs up user by email and password. Returns user id as int64 integer and access token on success or error on failed attempt
	SignUp(email, password string) (int64, string, error)

	// Logs in user by email and password. Returns user struct pointer and access token on success or error on failed attempt
	Login(email, password string) (*User, string, error)

	// Sends reset password link on email and returns reset password token or error on failure
	ResetPassword(email string) (string, error)

	// Updates password for existing user
	SetPassword(userID int64, password, resetToken string) error

	// Logs out user provided access token and returns error on failure
	Logout(accessToken string) error
}

type AuthValidator interface {
	// Validates authorization of user identified by id
	Validate(userID int64, accessToken string) error
}

type AuthConfig struct {
	Mailer mailer.Mailer
	UserRepo UserRepository
	SessRepo UserSessionsRepository
	ResetRepo UserPwdResetRepository
	ResetPasswordUrl string
	ResetPwdFromEmail string
	ResetPwdEmailSubject string
}

type authApi struct {
	mailer mailer.Mailer
	userRepo UserRepository
	sessRepo UserSessionsRepository
	resetRepo UserPwdResetRepository
	forgotPasswordUrl string
	resetPwdFromEmail string
	resetPwdEmailSubject string
}

func NewAuthAPI(cfg AuthConfig) AuthAPI {
	return &authApi{
		mailer: cfg.Mailer,
		userRepo: cfg.UserRepo,
		sessRepo: cfg.SessRepo,
		resetRepo: cfg.ResetRepo,
		forgotPasswordUrl: cfg.ResetPasswordUrl,
		resetPwdFromEmail: cfg.ResetPwdFromEmail,
		resetPwdEmailSubject: cfg.ResetPwdEmailSubject,
	}
}

var (
	ErrAuthApiInvalidEmail = errors.New("Email is not valid.")
	ErrAuthApiPwdEncrypt = errors.New("Error encrypting passwords.")
	ErrAuthApiInvalidCredentials = errors.New("Email or password you provided are not valid.")
	ErrAuthApiWrongResetPwdToken = errors.New("Wrong password reset code.")
	ErrAuthApiWrongAuthToken = errors.New("Wrong authorization token.")
)

func (api *authApi) SignUp(email, password string) (int64, string, error) {
	if err := validateEmail(email); err != nil {
		return 0, "", err
	}

	// TODO: password requirements validation
	hash, err := hashPassword(password)
	if err != nil {
		return 0, "", err
	}

	user := &User{
		Email: email,
		Password: hash,
	}

	userID, err := api.userRepo.Create(user)
	if err != nil {
		return 0, "", err
	}

	token, err := api.grantAccessToken(userID)
	if err != nil {
		return 0, "", err
	}

	return userID, token, nil
}

func (api *authApi) Login(email, password string) (*User, string, error) {
	if err := validateEmail(email); err != nil {
		return nil, "", err
	}

	user, err := api.userRepo.ReadByEmail(email)
	if err != nil {
		return nil, "", err
	}

	if err := comparePasswords(user.Password, password); err != nil {
		return nil, "", ErrAuthApiInvalidCredentials
	}

	token, err := api.grantAccessToken(user.UserID)
	if err != nil {
		return nil, "", err
	}

	return user, token, nil
}

func (api *authApi) ResetPassword(email string) (string, error) {
	if err := validateEmail(email); err != nil {
		return "", err
	}

	user, err := api.userRepo.ReadByEmail(email)
	if err != nil {
		return "", err
	}

	token, err := api.resetRepo.Read(user.UserID)
	if err != nil {
		resetPwd := &UserPwdReset{
			UserID: user.UserID,
			Email: user.Email,
		}
		token, err = api.resetRepo.Create(resetPwd)
		if err != nil {
			return "", err
		}
	}

	if err := api.sendForgotPasswordEmail(user.Email, token, user.UserID); err != nil {
		return "", err
	}

	return token, nil
}

func (api *authApi) SetPassword(userID int64, password, resetToken string) error {
	_, err := api.userRepo.Read(userID)
	if err != nil {
		return err
	}

	resetTokenStored, err := api.resetRepo.Read(userID)
	if err != nil {
		return err
	}

	if resetToken != resetTokenStored {
		return ErrAuthApiWrongResetPwdToken
	}

	// TODO: password requirements validation
	hash, err := hashPassword(password)
	fmt.Println("hashPassword", hash)
	if err != nil {
		return err
	}

	userUpdate := &UserUpdate{
		Password: &hash,
	}
	if err := api.userRepo.Update(userID, userUpdate); err != nil {
		return err
	}

	return nil
}

func (api *authApi) Logout(accessToken string) error {
	return api.sessRepo.Delete(accessToken)
}

func (api *authApi) Validate(userID int64, accessToken string) error {
	sess, err := api.sessRepo.Read(accessToken)
	if err != nil {
		return err
	}

	if sess.UserID != userID {
		return ErrAuthApiWrongAuthToken
	}

	return nil
}

func (api *authApi) grantAccessToken(userID int64) (string, error) {
	userSess := &UserSession{
		UserID: userID,
	}
	return api.sessRepo.Create(userSess)
}

func (api *authApi) sendForgotPasswordEmail(email, token string, userID int64) error {
	return api.mailer.SendEmail(email, api.resetPwdFromEmail, api.resetPwdEmailSubject,
		"Click following link to reset password " + api.forgotPasswordUrl + "?token=" + token + "&id=" + strconv.FormatInt(userID, 10))
}
