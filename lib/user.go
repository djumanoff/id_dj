package lib

import "time"

// Struct describes user information
type User struct {
	UserID int64 `json:"user_id"`
	Email string `json:"email,omitempty"`
	FirstName string `json:"first_name,omitempty"`
	LastName string `json:"last_name,omitempty"`
	Address string `json:"address,omitempty"`
	PhoneNumber string `json:"phone_number,omitempty"`
	Password string `json:"-"`
	CoordsLat float64 `json:"coords_lat,omitempty"`
	CoordsLng float64 `json:"coords_lng,omitempty"`
	CreatedAt time.Time `json:"created_at,omitempty"`
}

type ProfileUpdate struct {
	Email *string `json:"email,omitempty"`
	FirstName *string `json:"first_name,omitempty"`
	LastName *string `json:"last_name,omitempty"`
	Address *string `json:"address,omitempty"`
	PhoneNumber *string `json:"phone_number,omitempty"`
	CoordsLat *float64 `json:"coords_lat,omitempty"`
	CoordsLng *float64 `json:"coords_lng,omitempty"`
}

// Struct describes user information that can be updated
type UserUpdate struct {
	*ProfileUpdate
	//Email *string `json:"email,omitempty"`
	//FirstName *string `json:"first_name,omitempty"`
	//LastName *string `json:"last_name,omitempty"`
	//Address *string `json:"address,omitempty"`
	Password *string `json:"-"`
	//PhoneNumber *string `json:"phone_number,omitempty"`
	//CoordsLat *float64 `json:"coords_lat,omitempty"`
	//CoordsLng *float64 `json:"coords_lng,omitempty"`
}
