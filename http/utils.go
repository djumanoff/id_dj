package http

import (
	"encoding/json"
	"net/http"
	"fmt"
)

func ParseJSON(r *http.Request, item interface{}) error {
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	err := decoder.Decode(item)
	if err != nil {
		return err
	}

	fmt.Println(item)

	return nil
}
