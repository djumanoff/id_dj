package api

import (
	"bitbucket.org/djumanoff/id_dj/lib"
	htp "bitbucket.org/djumanoff/id_dj/http"
	"net/http"
	"time"
	"encoding/json"
	"bytes"
	"io/ioutil"
)

type (
	authApiClient struct {
		clt *http.Client
		baseUrl string
	}
)

func NewAuthAPI(baseUrl string) (lib.AuthAPI, error) {
	apiObj := &authApiClient{}
	apiObj.baseUrl = baseUrl
	apiObj.clt = &http.Client{
		Timeout: 30 * time.Second,
	}
	return apiObj, nil
}

// Signs up user by email and password. Returns user id as int64 integer and access token on success or error on failed attempt
func (api *authApiClient) SignUp(email, password string) (int64, string, error) {
	reqUrl := api.baseUrl + "/api/users"

	// definition of structs used for marshaling/unmarshaling
	type (
		authRequest struct {
			Email string `json:"email,omitempty"`
			Password string `json:"password,omitempty"`
		}
		signupResponse struct {
			UserID int64 `json:"user_id,omitempty"`
			AccessToken string `json:"access_token,omitempty"`
		}
	)

	userUpdate := &authRequest{
		Email: email,
		Password: password,
	}
	reqData, err := json.Marshal(userUpdate)
	if err != nil {
		return 0, "", err
	}

	req, err := http.NewRequest("POST", reqUrl, bytes.NewBuffer(reqData))
	if err != nil {
		return 0, "", err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := api.clt.Do(req)
	if err != nil {
		return 0, "", err
	}
	defer resp.Body.Close()

	d, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, "", err
	}

	if err := htp.CheckIfErrResponse(resp, d); err != nil {
		return 0, "", err
	}

	signupResp := &signupResponse{}
	err = json.Unmarshal(d, signupResp)
	if err != nil {
		return 0, "", err
	}

	return signupResp.UserID, signupResp.AccessToken, nil
}

// Logs in user by email and password. Returns user struct pointer and access token on success or error on failed attempt
func (api *authApiClient) Login(email, password string) (*lib.User, string, error) {
	reqUrl := api.baseUrl + "/api/auth/token"

	// definition of structs used for marshaling/unmarshaling
	type (
		authRequest struct {
			Email string `json:"email,omitempty"`
			Password string `json:"password,omitempty"`
		}
		loginResponse struct {
			lib.User
			AccessToken string `json:"access_token,omitempty"`
		}
	)

	authData := &authRequest{
		Email: email,
		Password: password,
	}
	reqData, err := json.Marshal(authData)
	if err != nil {
		return nil, "", err
	}

	req, err := http.NewRequest("POST", reqUrl, bytes.NewBuffer(reqData))
	if err != nil {
		return nil, "", err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := api.clt.Do(req)
	if err != nil {
		return nil, "", err
	}
	defer resp.Body.Close()

	d, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, "", err
	}

	if err := htp.CheckIfErrResponse(resp, d); err != nil {
		return nil, "", err
	}

	loginResp := &loginResponse{}
	err = json.Unmarshal(d, loginResp)
	if err != nil {
		return nil, "", err
	}

	return &loginResp.User, loginResp.AccessToken, nil
}

// Sends reset password link on email and returns reset password token or error on failure
func (api *authApiClient) ResetPassword(email string) (string, error) {
	reqUrl := api.baseUrl + "/api/auth/password/reset"

	// definition of structs used for marshaling/unmarshaling
	type (
		resetPasswordRequest struct {
			Email string `json:"email,omitempty"`
		}
		resetPasswordResponse struct {
			ResetToken string `json:"reset_token,omitempty"`
		}
	)

	authData := &resetPasswordRequest{
		Email: email,
	}
	reqData, err := json.Marshal(authData)
	if err != nil {
		return "", err
	}

	req, err := http.NewRequest("POST", reqUrl, bytes.NewBuffer(reqData))
	if err != nil {
		return "", err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := api.clt.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	d, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	if err := htp.CheckIfErrResponse(resp, d); err != nil {
		return "", err
	}

	resetResp := &resetPasswordResponse{}
	err = json.Unmarshal(d, resetResp)
	if err != nil {
		return "", err
	}

	return resetResp.ResetToken, nil
}

// Updates password for existing user
func (api *authApiClient) SetPassword(userID int64, password, resetToken string) error {
	reqUrl := api.baseUrl + "/api/auth/password"

	// definition of structs used for marshaling/unmarshaling
	type (
		setPasswordRequest struct {
			UserID int64 `json:"user_id,omitempty"`
			ResetToken string `json:"reset_token,omitempty"`
			Password string `json:"password,omitempty"`
		}
		setPasswordResponse struct {}
	)

	authData := &setPasswordRequest{
		UserID: userID,
		ResetToken: resetToken,
		Password: password,
	}
	reqData, err := json.Marshal(authData)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("PUT", reqUrl, bytes.NewBuffer(reqData))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := api.clt.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	d, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if err := htp.CheckIfErrResponse(resp, d); err != nil {
		return err
	}

	setResp := &setPasswordResponse{}
	err = json.Unmarshal(d, setResp)
	if err != nil {
		return err
	}

	return nil
}

// Logs out user provided access token and returns error on failure
func (api *authApiClient) Logout(accessToken string) error {
	reqUrl := api.baseUrl + "/api/auth/token/" + accessToken

	// definition of structs used for marshaling/unmarshaling
	type (
		logoutResponse struct {}
	)
	req, err := http.NewRequest("DELETE", reqUrl, nil)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := api.clt.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	d, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if err := htp.CheckIfErrResponse(resp, d); err != nil {
		return err
	}

	setResp := &logoutResponse{}
	err = json.Unmarshal(d, setResp)
	if err != nil {
		return err
	}

	return nil
}
