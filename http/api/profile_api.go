package api

import (
	"bitbucket.org/djumanoff/id_dj/lib"
	htp "bitbucket.org/djumanoff/id_dj/http"
	"net/http"
	"time"
	"encoding/json"
	"bytes"
	"io/ioutil"
	"strconv"
	"fmt"
)

type (
	profileApiClient struct {
		clt *http.Client
		baseUrl string
	}
)

func NewProfileAPI(baseUrl string) (lib.ProtectedProfileAPI, error) {
	apiObj := &profileApiClient{}
	apiObj.baseUrl = baseUrl
	apiObj.clt = &http.Client{
		Timeout: 30 * time.Second,
	}
	return apiObj, nil
}

// Returns user profile in user struct
func (api *profileApiClient) GetProfile(userID int64, accessToken string) (*lib.User, error) {
	reqUrl := api.baseUrl + "/api/users/" + strconv.FormatInt(userID, 10)

	req, err := http.NewRequest("GET", reqUrl, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", accessToken)

	resp, err := api.clt.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	d, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if err := htp.CheckIfErrResponse(resp, d); err != nil {
		return nil, err
	}

	user := &lib.User{}
	err = json.Unmarshal(d, user)
	if err != nil {
		return nil, err
	}

	return user, nil
}

// Updates user profile (first name, last name, address)
func (api *profileApiClient) UpdateProfile(userID int64, userInfo *lib.ProfileUpdate, accessToken string) (*lib.User, error) {
	reqUrl := api.baseUrl + "/api/users/" + strconv.FormatInt(userID, 10)

	reqData, err := json.Marshal(userInfo)
	if err != nil {
		return nil, err
	}

	fmt.Println("reqData", string(reqData))
	fmt.Println("reqUrl", reqUrl)

	req, err := http.NewRequest("PUT", reqUrl, bytes.NewBuffer(reqData))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", accessToken)

	resp, err := api.clt.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	d, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if err := htp.CheckIfErrResponse(resp, d); err != nil {
		return nil, err
	}

	user := &lib.User{}
	err = json.Unmarshal(d, user)
	if err != nil {
		return nil, err
	}

	return user, nil
}
