package http

import (
	"net/http"
	"encoding/json"
	"io/ioutil"
	"bytes"
	"fmt"
	"strings"
	l "bitbucket.org/djumanoff/id_dj/logger"
	"time"
	"bitbucket.org/djumanoff/id_dj/lib"
	"github.com/gorilla/mux"
	"strconv"
)

type HttpHandler func(w http.ResponseWriter, r *http.Request)

type HttpEndpoint func(w http.ResponseWriter, r *http.Request) HttpResponse

func Auth(fn HttpEndpoint, auth lib.AuthValidator, userIdParam string) HttpEndpoint {
	return func(w http.ResponseWriter, r *http.Request) HttpResponse {
		vars := mux.Vars(r)
		idStr, ok := vars[userIdParam]
		if !ok {
			return &HttpErr{400, "No user id param"}
		}

		id, err := strconv.ParseInt(idStr, 10, 64)
		if err != nil {
			return &HttpErr{400, "User id is not valid integer"}
		}

		accessToken := r.Header.Get("Authorization")
		if err = auth.Validate(id, accessToken); err != nil {
			return &HttpErr{403, "Access forbidden"}
		}

		return fn(w, r)
	}
}

func Json(fn HttpEndpoint) HttpHandler {
	return func(w http.ResponseWriter, r *http.Request) {
		d := fn(w, r)

		w.Header().Set("Content-Type", "application/json; charset=UTF-8")

		for k, v := range d.Headers() {
			w.Header().Set(k, v)
		}

		w.WriteHeader(d.StatusCode())
		json.NewEncoder(w).Encode(d.Response())
	}
}

func Logging(log l.Logger, fn HttpEndpoint) HttpEndpoint {
	return func(w http.ResponseWriter, r *http.Request) HttpResponse {
		start := time.Now()

		// Read the content
		var bodyBytes []byte
		if r.Body != nil {
			bodyBytes, _ = ioutil.ReadAll(r.Body)
		}
		// Restore the io.ReadCloser to its original state
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
		// Use the content
		bodyString := string(bodyBytes)

		d := fn(w, r)
		dBytes, _ := json.Marshal(d.Response())

		if d.StatusCode()%200 < 100 || d.StatusCode()%300 < 100 {
			log.Warn(LogRequest(r), " body= ", bodyString, time.Since(start), " ", d.StatusCode(), " ", string(dBytes))
		} else {
			log.Debug(LogRequest(r), " body= ", bodyString, time.Since(start), " ", d.StatusCode(), " ", string(dBytes))
		}

		return d
	}
}

func LogRequest(r *http.Request) string {
	// Create return string
	var request []string
	// Add the request string
	urlPath := fmt.Sprintf("%v %v", r.Method, r.URL)
	request = append(request, urlPath)
	// Add the host
	request = append(request, fmt.Sprintf("Host: %v", r.Host))
	// Loop through headers
	for name, headers := range r.Header {
		name = strings.ToLower(name)
		for _, h := range headers {
			request = append(request, fmt.Sprintf("%v: %v", name, h))
		}
	}

	// If this is a POST, add post data
	if r.Method == "POST" {
		_ = r.ParseForm()
		request = append(request, " ")
		request = append(request, r.Form.Encode())
	}
	// Return the request as a string
	return strings.Join(request, " ") + " "
}
