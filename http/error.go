package http

import (
	"encoding/json"
	"net/http"
)

var emptyHeaders = map[string]string{}

type HttpErr struct {
	Code int `json:"status,omitempty"`
	Message string `json:"message,omitempty"`
}

func (err HttpErr) Error() string {
	return err.Message
}

func (err HttpErr) Response() interface{} {
	return err
}

func (err HttpErr) StatusCode() int {
	return err.Code
}

func (err HttpErr) Headers() map[string]string {
	return emptyHeaders
}

func CheckIfErrResponse(resp *http.Response, d []byte) error {
	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		return nil
	}

	httpErr := &HttpErr{}
	err := json.Unmarshal(d, httpErr)
	if err != nil {
		return err
	}
	return httpErr
}
