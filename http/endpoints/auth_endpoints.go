package endpoints

import (
	"bitbucket.org/djumanoff/id_dj/cqrs"
	htp "bitbucket.org/djumanoff/id_dj/http"
	"net/http"
	"github.com/gorilla/mux"
)

type HttpAuthEndpointFactory struct {
	ch cqrs.AuthCommandHandler
}

func NewHttpAuthEndpointFactory(ch cqrs.AuthCommandHandler) *HttpAuthEndpointFactory {
	return &HttpAuthEndpointFactory{ch}
}

func (fac *HttpAuthEndpointFactory) MakeNotFoundEndpoint() htp.HttpEndpoint {
	return func(w http.ResponseWriter, r *http.Request) htp.HttpResponse {
		return &htp.HttpErr{404, "Route not found: "+r.URL.Path}
	}
}

func (fac *HttpAuthEndpointFactory) MakeLoginEndpoint() htp.HttpEndpoint {
	return func(w http.ResponseWriter, r *http.Request) htp.HttpResponse {
		cmd := &cqrs.LoginCommand{}
		err := htp.ParseJSON(r, cmd)
		if err != nil {
			return &htp.HttpErr{400, err.Error()}
		}

		result, err := fac.ch.ExecCommand(cmd)
		if err != nil {
			return &htp.HttpErr{400, err.Error()}
		}

		return &htp.Response{200, result, nil}
	}
}

func (fac *HttpAuthEndpointFactory) MakeSignUpEndpoint() htp.HttpEndpoint {
	return func(w http.ResponseWriter, r *http.Request) htp.HttpResponse {
		cmd := &cqrs.SignUpCommand{}
		err := htp.ParseJSON(r, cmd)
		if err != nil {
			return &htp.HttpErr{400, err.Error()}
		}

		result, err := fac.ch.ExecCommand(cmd)
		if err != nil {
			return &htp.HttpErr{400, err.Error()}
		}

		return &htp.Response{200, result, nil}
	}
}

func (fac *HttpAuthEndpointFactory) MakeResetPasswordEndpoint() htp.HttpEndpoint {
	return func(w http.ResponseWriter, r *http.Request) htp.HttpResponse {
		cmd := &cqrs.ResetPasswordCommand{}
		err := htp.ParseJSON(r, cmd)
		if err != nil {
			return &htp.HttpErr{400, err.Error()}
		}

		result, err := fac.ch.ExecCommand(cmd)
		if err != nil {
			return &htp.HttpErr{400, err.Error()}
		}

		return &htp.Response{200, result, nil}
	}
}

func (fac *HttpAuthEndpointFactory) MakeSetPasswordEndpoint() htp.HttpEndpoint {
	return func(w http.ResponseWriter, r *http.Request) htp.HttpResponse {
		cmd := &cqrs.SetPasswordCommand{}
		err := htp.ParseJSON(r, cmd)
		if err != nil {
			return &htp.HttpErr{400, err.Error()}
		}

		result, err := fac.ch.ExecCommand(cmd)
		if err != nil {
			return &htp.HttpErr{400, err.Error()}
		}

		return &htp.Response{200, result, nil}
	}
}

func (fac *HttpAuthEndpointFactory) MakeLogoutEndpoint(tokenParam string) htp.HttpEndpoint {
	return func(w http.ResponseWriter, r *http.Request) htp.HttpResponse {
		vars := mux.Vars(r)
		token, ok := vars[tokenParam]
		if !ok {
			return &htp.HttpErr{400, "No user id param"}
		}

		cmd := &cqrs.LogoutCommand{token}
		result, err := fac.ch.ExecCommand(cmd)
		if err != nil {
			return &htp.HttpErr{400, err.Error()}
		}

		return &htp.Response{200, result, nil}
	}
}
