package endpoints

import (
	"bitbucket.org/djumanoff/id_dj/cqrs"
	htp "bitbucket.org/djumanoff/id_dj/http"
	"net/http"
	"github.com/gorilla/mux"
	"strconv"
	"bitbucket.org/djumanoff/id_dj/lib"
)

type HttpProfileEndpointFactory struct {
	ch cqrs.ProfileCommandHandler
}

func NewHttpProfileEndpointFactory(ch cqrs.ProfileCommandHandler) *HttpProfileEndpointFactory {
	return &HttpProfileEndpointFactory{ch}
}

func (fac *HttpProfileEndpointFactory) MakeGetProfileEndpoint(userIdParam string) htp.HttpEndpoint {
	return func(w http.ResponseWriter, r *http.Request) htp.HttpResponse {
		vars := mux.Vars(r)
		idStr, ok := vars[userIdParam]
		if !ok {
			return &htp.HttpErr{400, "No user id param"}
		}

		id, err := strconv.ParseInt(idStr, 10, 64)
		if err != nil {
			return &htp.HttpErr{400, "User id is not valid integer"}
		}

		cmd := &cqrs.GetProfileCommand{id}
		result, err := fac.ch.ExecCommand(cmd)
		if err != nil {
			return &htp.HttpErr{400, err.Error()}
		}

		return &htp.Response{200, result, nil}
	}
}

func (fac *HttpProfileEndpointFactory) MakeUpdateProfileEndpoint(userIdParam string) htp.HttpEndpoint {
	return func(w http.ResponseWriter, r *http.Request) htp.HttpResponse {
		vars := mux.Vars(r)
		idStr, ok := vars[userIdParam]
		if !ok {
			return &htp.HttpErr{400, "No user id param"}
		}

		id, err := strconv.ParseInt(idStr, 10, 64)
		if err != nil {
			return &htp.HttpErr{400, "User id is not valid integer"}
		}

		upd := &lib.ProfileUpdate{}
		if err := htp.ParseJSON(r, upd); err != nil {
			return &htp.HttpErr{400, err.Error()}
		}

		cmd := &cqrs.UpdateProfileCommand{}
		cmd.UserID = id
		cmd.ProfileUpdate = upd

		result, err := fac.ch.ExecCommand(cmd)
		if err != nil {
			return &htp.HttpErr{400, err.Error()}
		}

		return &htp.Response{200, result, nil}
	}
}
