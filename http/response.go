package http

type HttpResponse interface {
	Headers() map[string]string
	Response() interface{}
	StatusCode() int
}

type Response struct {
	Status     int
	Data       interface{}
	HeaderData map[string]string
}

func (e Response) Response() interface{} {
	return e.Data
}

func (e Response) StatusCode() int {
	return e.Status
}

func (e Response) Headers() map[string]string {
	return e.HeaderData
}
