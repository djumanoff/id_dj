package main

import (
	"github.com/urfave/cli"
	"os"
	"errors"
	"github.com/joho/godotenv"
	"bitbucket.org/djumanoff/id_dj/backend"
	"bitbucket.org/djumanoff/id_dj/frontend"
	"strconv"
	"fmt"
)

// Varibales that hold configuration
var (
	// Full database url (mysql://sandy:secret@myhost1:1111/db)
	dbUrl = ""

	// Base url of the backend API
	apiBaseUrl = ""

	// Reset password email subject
	resetPwdEmailSubject = ""

	// Reset password email from
	resetPwdEmailFrom = ""

	// Endpoint to bind the http server (0.0.0.0:8080)
	port = ""

	// Path to .env config file
	configPath = ""

	smtpHost = ""

	smtpUser = ""

	smtpPassword = ""

	smtpPort = ""

	version = "1.0.0"

	// Type of the sever (backend, frontend)
	serverType = ""
)

// Errors
var (
	ErrNoEndpoint = errors.New("Endpoint to listen not provided")
	ErrNoDatabaseUrl = errors.New("Database url not provided")
)

var flags = []cli.Flag{
	cli.StringFlag{
		Name:        "config, c",
		Value:       "",
		Usage:       "path to .env config file",
		Destination: &configPath,
	},
	cli.StringFlag{
		Name:        "db_url, dbu",
		Value:       os.Getenv("DB_URL"),
		Usage:       "url of MySQL database",
		Destination: &dbUrl,
	},
	cli.StringFlag{
		Name:        "api_base_url, abu",
		Value:       os.Getenv("API_BASE_URL"),
		Usage:       "Base url of the backend API",
		Destination: &apiBaseUrl,
	},
	cli.StringFlag{
		Name:        "reset_email_subject, rpes",
		Value:       os.Getenv("RESET_PWD_EMAIL_SUBJECT"),
		Usage:       "Reset password email subject",
		Destination: &resetPwdEmailSubject,
	},
	cli.StringFlag{
		Name:        "reset_email_from, rpef",
		Value:       os.Getenv("RESET_PWD_EMAIL_FROM"),
		Usage:       "Reset password email subject",
		Destination: &resetPwdEmailFrom,
	},
	cli.StringFlag{
		Name:        "smtp_host, sh",
		Value:       os.Getenv("SMTP_HOST"),
		Usage:       "SMTP host for email sending",
		Destination: &smtpHost,
	},
	cli.StringFlag{
		Name:        "smtp_port, sp",
		Value:       os.Getenv("SMTP_PORT"),
		Usage:       "SMTP port for email sending",
		Destination: &smtpPort,
	},
	cli.StringFlag{
		Name:        "smtp_user, su",
		Value:       os.Getenv("SMTP_USER"),
		Usage:       "SMTP user for email sending",
		Destination: &smtpUser,
	},
	cli.StringFlag{
		Name:        "smtp_password, spwd",
		Value:       os.Getenv("SMTP_PASSWORD"),
		Usage:       "SMTP user password for email sending",
		Destination: &smtpPassword,
	},
	cli.StringFlag{
		Name:        "server_type, srv_t",
		Value:       os.Getenv("SERVER_TYPE"),
		Usage:       "Type of the sever (backend, frontend)",
		Destination: &serverType,
	},
	cli.StringFlag{
		Name:        "port, p",
		Value:       os.Getenv("PORT"),
		Usage:       "port to listen by the web server",
		Destination: &port,
	},
}

var cmds = []cli.Command{
	{
		Name:    "backend",
		Usage:   "run backend REST API HTTP server",
		Action:  runBackend,
	},
	{
		Name:    "frontend",
		Usage:   "run frontend web server",
		Action:  runFrontend,
	},
}

func main() {
	app := cli.NewApp()
	app.Name = "ID_DJ"
	app.Usage = "user signup/login demo app"
	app.UsageText = "id_dj [command] [global options]"
	app.Version = version
	app.Flags = flags
	app.Commands = cmds
	app.Action = run

	fmt.Println(app.Run(os.Args))
}

func run(ctx *cli.Context) error {
	parseEnvFile()

	if serverType == "backend" {
		return runBackend(ctx)
	}

	return runFrontend(ctx)
}

func runBackend(*cli.Context) error {
	parseEnvFile()

	if port == "" {
		return errors.New("Port equals " + port)
	}

	if dbUrl == "" {
		return ErrNoDatabaseUrl
	}

	smtpPortInt, err := strconv.Atoi(smtpPort)
	if err != nil {
		smtpPortInt = 25
	}

	return backend.Run(backend.Config{
		Endpoint: ":" + port,
		DatabaseUrl: dbUrl,
		EmailSettings: backend.EmailSettings{
			Host: smtpHost,
			Port: smtpPortInt,
			Username: smtpUser,
			Password: smtpPassword,
		},
	})
}

func runFrontend(*cli.Context) error {
	parseEnvFile()

	if port == "" {
		return ErrNoEndpoint
	}

	return frontend.Run(frontend.Config{
		Endpoint: ":" + port,
		APIBaseUrl: apiBaseUrl,
	})
}

// Loads configuration from .env to corresponding environment variables,
// so server can be configured either from config file or directly by passing env vars
func parseEnvFile() {
	if configPath != "" {
		_ = godotenv.Load(configPath)
	}

	port = os.Getenv("PORT")
	dbUrl = os.Getenv("DB_URL")
	apiBaseUrl = os.Getenv("API_BASE_URL")
	resetPwdEmailSubject = os.Getenv("RESET_PWD_EMAIL_SUBJECT")
	resetPwdEmailFrom = os.Getenv("RESET_PWD_EMAIL_FROM")
	smtpHost = os.Getenv("SMTP_HOST")
	smtpPort = os.Getenv("SMTP_PORT")
	smtpUser = os.Getenv("SMTP_USER")
	smtpPassword = os.Getenv("SMTP_PASSWORD")
}
