package backend

import (
	"bitbucket.org/djumanoff/id_dj/mailer"
	"bitbucket.org/djumanoff/id_dj/lib"
	"bitbucket.org/djumanoff/id_dj/db/mysql"
	"bitbucket.org/djumanoff/id_dj/cqrs"
	"bitbucket.org/djumanoff/id_dj/http/endpoints"
	htp "bitbucket.org/djumanoff/id_dj/http"
	"github.com/gorilla/mux"
	"bitbucket.org/djumanoff/id_dj/logger"
	"net/http"
)

type EmailSettings struct {
	Host string
	Port int
	Username string
	Password string
}

type Config struct {
	Endpoint string
	DatabaseUrl string
	EmailSettings EmailSettings
	ResetPasswordEmailSubject string
	ResetPasswordFromEmail string
	ResetPasswordUrl string
}

func Run(cfg Config) error {
	conn, err := mysql.NewConnection(cfg.DatabaseUrl)
	if err != nil {
		return err
	}

	// Initialize tables at first run
	mysql.InitDB(conn)

	mailServer := mailer.New(mailer.Config{
		Host: cfg.EmailSettings.Host,
		Port: cfg.EmailSettings.Port,
		Username: cfg.EmailSettings.Username,
		Password: cfg.EmailSettings.Password,
	})

	userRepo := mysql.NewUserRepo(conn)
	sessRepo := mysql.NewSessionRepo(conn)
	pwdResetRepo := mysql.NewPwdResetRepo(conn)

	authApi := lib.NewAuthAPI(lib.AuthConfig{
		Mailer: mailServer,
		UserRepo: userRepo,
		SessRepo: sessRepo,
		ResetRepo: pwdResetRepo,
		ResetPasswordUrl: cfg.ResetPasswordUrl,
		ResetPwdEmailSubject: cfg.ResetPasswordEmailSubject,
		ResetPwdFromEmail: cfg.ResetPasswordFromEmail,
	})

	profileApi := lib.NewProfileAPI(lib.ProfileConfig{
		UserRepo: userRepo,
	})

	authCmdHandler := cqrs.NewAuthCommandHandler(authApi)
	profileCmdHandler := cqrs.NewProfileCommandHandler(profileApi)

	authEndpointFactory := endpoints.NewHttpAuthEndpointFactory(authCmdHandler)
	profileEndpointFactory := endpoints.NewHttpProfileEndpointFactory(profileCmdHandler)

	r := mux.NewRouter()
	log := logger.New("backend")

	r.HandleFunc("/api/users",
		htp.Json(
			htp.Logging(log, authEndpointFactory.MakeSignUpEndpoint())),
	).Methods("POST")

	r.HandleFunc("/api/auth/token",
		htp.Json(
			htp.Logging(log, authEndpointFactory.MakeLoginEndpoint())),
	).Methods("POST")

	r.HandleFunc("/api/auth/password/reset",
		htp.Json(
			htp.Logging(log, authEndpointFactory.MakeResetPasswordEndpoint())),
	).Methods("POST")

	r.HandleFunc("/api/auth/password",
		htp.Json(
			htp.Logging(log, authEndpointFactory.MakeSetPasswordEndpoint())),
	).Methods("PUT")

	r.HandleFunc("/api/auth/token/{token}",
		htp.Json(
			htp.Logging(log, authEndpointFactory.MakeLogoutEndpoint("token"))),
	).Methods("DELETE")

	r.HandleFunc("/api/users/{user_id}",
		htp.Json(
			htp.Logging(log,
				htp.Auth(profileEndpointFactory.MakeGetProfileEndpoint("user_id"), authApi.(lib.AuthValidator), "user_id"),
			),
		),
	).Methods("GET")

	r.HandleFunc("/api/users/{user_id}",
		htp.Json(
			htp.Logging(log,
				htp.Auth(profileEndpointFactory.MakeUpdateProfileEndpoint("user_id"), authApi.(lib.AuthValidator), "user_id"),
			),
		),
	).Methods("PUT")

	r.NotFoundHandler = http.HandlerFunc(htp.Json(htp.Logging(log, authEndpointFactory.MakeNotFoundEndpoint())))

	return http.ListenAndServe(cfg.Endpoint, r)
}
