# ID_DJ

This repository contains two server applications:

1. Backend HTTP REST API, which handles all domain logic

2. Frontend web server, which renders UI and communicate with backend API server

Help of the compiled binary 

 ```$xslt
NAME:
   ID_DJ - user signup/login demo app

USAGE:
   id_dj [command] [global options]

VERSION:
   1.0.0

COMMANDS:
     backend   run backend REST API HTTP server
     frontend  run frontend web server
     help, h   Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --config value, -c value                   path to .env config file
   --db_url value, --dbu value                url of MySQL database
   --api_base_url value, --abu value          Base url of the backend API
   --reset_email_subject value, --rpes value  Reset password email subject
   --reset_email_from value, --rpef value     Reset password email subject
   --smtp_host value, --sh value              SMTP host for email sending
   --smtp_port value, --sp value              SMTP port for email sending
   --smtp_user value, --su value              SMTP user for email sending
   --smtp_password value, --spwd value        SMTP user password for email sending
   --endpoint value, -e value                 endpoint to listen by the web server
   --help, -h                                 show help
   --version, -v                              print the version
```
