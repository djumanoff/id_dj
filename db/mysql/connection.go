package mysql

import "database/sql"

func NewConnection(sqlUrl string) (*sql.DB, error) {
	db, err := sql.Open("mysql", sqlUrl)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, nil
}

func NewSqlLiteConnection(sqlUrl string) (*sql.DB, error) {
	db, err := sql.Open("sqlite3", sqlUrl)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, nil
}

func InitDB(db *sql.DB) error {
	for _, q := range queries {
		db.Exec(q)
	}
	return nil
}
