package mysql

var (
	queries = []string{`
CREATE TABLE IF NOT EXISTS user_password_resets (
  user_id int(11) unsigned NOT NULL,
  reset_token varchar(16) DEFAULT NULL,
  email varchar(255) DEFAULT NULL,
  created_at datetime DEFAULT NULL,
  valid_until datetime DEFAULT NULL,
  PRIMARY KEY (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
`,`
CREATE TABLE IF NOT EXISTS user_sessions (
  access_token char(36) NOT NULL,
  user_id int(11) unsigned NOT NULL DEFAULT '0',
  created_at datetime DEFAULT NULL,
  valid_until datetime DEFAULT NULL,
  PRIMARY KEY (access_token)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;
`,`
CREATE TABLE IF NOT EXISTS users (
  user_id int(11) unsigned NOT NULL AUTO_INCREMENT,
  email varchar(255) DEFAULT NULL,
  password varchar(255) DEFAULT NULL,
  first_name varchar(255) DEFAULT NULL,
  last_name varchar(255) DEFAULT NULL,
  address varchar(255) DEFAULT NULL,
  coords_lat decimal(10,8) NOT NULL DEFAULT '0.00000000',
  coords_lng decimal(10,8) NOT NULL DEFAULT '0.00000000',
  created_at datetime DEFAULT NULL,
  phone_number varchar(255) DEFAULT NULL,
  PRIMARY KEY (user_id),
  UNIQUE KEY email_2 (email),
  KEY email (email)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
`,
}
)
