package mysql

import (
	"bitbucket.org/djumanoff/id_dj/lib"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"time"
	"github.com/gofrs/uuid"
)

type sessionRepo struct {
	db *sql.DB
}

func NewSessionRepo(db *sql.DB) lib.UserSessionsRepository {
	return &sessionRepo{db:db}
}

func (repo *sessionRepo) Create(userSession *lib.UserSession) (string, error) {
	token := generateAccessToken()
	_, err := repo.db.Exec("INSERT INTO user_sessions (access_token, user_id, created_at, valid_until) VALUES (?, ?, ?, ?)",
		token, userSession.UserID, time.Now(), time.Now().Add(24 * time.Hour))

	if err != nil {
		return "", err
	}

	return token, nil
}

func (repo *sessionRepo) Read(token string) (*lib.UserSession, error) {
	userSess := &lib.UserSession{}

	err := repo.db.QueryRow("SELECT user_id, created_at, valid_until FROM user_sessions WHERE access_token = ?", token).
		Scan(&userSess.UserID, &userSess.CreatedAt, &userSess.ValidUntil)

	if err != nil {
		return nil, err
	}

	return userSess, nil
}

func (repo *sessionRepo) Delete(token string) error {
	_, err := repo.db.Exec("DELETE FROM user_sessions WHERE access_token = ?", token)
	if err != nil {
		return err
	}
	return nil
}


func generateAccessToken() string {
	u, _ := uuid.NewV4()
	return u.String()
}
