package mysql

import (
	"bitbucket.org/djumanoff/id_dj/lib"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"time"
	"errors"
	"strings"
)

var (
	ErrEmptyUpdate = errors.New("nothing to update")
)

type userRepo struct {
	db *sql.DB
}

func NewUserRepo(db *sql.DB) lib.UserRepository {
	return &userRepo{db:db}
}

func (repo *userRepo) Create(user *lib.User) (int64, error) {
	result, err := repo.db.Exec(
		"INSERT INTO users (email, password, first_name, last_name, address, coords_lat, coords_lng, created_at, phone_number) " +
			"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
		user.Email, user.Password, user.FirstName, user.LastName, user.Address, user.CoordsLat, user.CoordsLng, time.Now(), user.PhoneNumber)

	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (repo *userRepo) Update(userID int64, userUpdate *lib.UserUpdate) error {
	q := "UPDATE users SET "
	vals := []interface{}{}
	qs := []string{}

	if userUpdate.Email != nil {
		qs = append(qs, "email = ?")
		vals = append(vals, *userUpdate.Email)
	}

	if userUpdate.FirstName != nil {
		qs = append(qs, "first_name = ?")
		vals = append(vals, *userUpdate.FirstName)
	}

	if userUpdate.LastName != nil {
		qs = append(qs, "last_name = ?")
		vals = append(vals, *userUpdate.LastName)
	}

	if userUpdate.Password != nil {
		qs = append(qs, "password = ?")
		vals = append(vals, *userUpdate.Password)
	}

	if userUpdate.Address != nil {
		qs = append(qs, "address = ?")
		vals = append(vals, *userUpdate.Address)
	}

	if userUpdate.CoordsLng != nil {
		qs = append(qs, "coords_lng = ?")
		vals = append(vals, *userUpdate.CoordsLng)
	}

	if userUpdate.CoordsLat != nil {
		qs = append(qs, "coords_lat = ?")
		vals = append(vals, *userUpdate.CoordsLat)
	}

	if userUpdate.PhoneNumber != nil {
		qs = append(qs, "phone_number = ?")
		vals = append(vals, *userUpdate.PhoneNumber)
	}

	if len(vals) <= 0 {
		return ErrEmptyUpdate
	}

	q += strings.Join(qs, ", ")
	q += " WHERE user_id = ? LIMIT 1"
	vals = append(vals, userID)

	_, err := repo.db.Exec(q, vals...)
	if err != nil {
		return err
	}

	return nil
}

func (repo *userRepo) Read(userID int64) (*lib.User, error) {
	user := &lib.User{}

	err := repo.db.QueryRow("SELECT user_id, email, password, first_name, last_name, address, coords_lat, coords_lng, created_at, phone_number FROM users WHERE user_id = ?",userID).
		Scan(&user.UserID, &user.Email, &user.Password, &user.FirstName, &user.LastName, &user.Address, &user.CoordsLat, &user.CoordsLng, &user.CreatedAt, &user.PhoneNumber)

	if err != nil {
		return nil, err
	}

	return user, nil
}

func (repo *userRepo) ReadByEmail(email string) (*lib.User, error) {
	user := &lib.User{}

	err := repo.db.QueryRow("SELECT user_id, email, password, first_name, last_name, address, coords_lat, coords_lng, created_at, phone_number FROM users WHERE email = ?", email).
		Scan(&user.UserID, &user.Email, &user.Password, &user.FirstName, &user.LastName, &user.Address, &user.CoordsLat, &user.CoordsLng, &user.CreatedAt, &user.PhoneNumber)

	if err != nil {
		return nil, err
	}

	return user, nil
}

func (repo *userRepo) Delete(userID int64) error {
	_, err := repo.db.Exec("DELETE FROM users WHERE user_id = ?", userID)
	if err != nil {
		return err
	}
	return nil
}
