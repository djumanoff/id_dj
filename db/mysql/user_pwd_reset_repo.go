package mysql

import (
	"bitbucket.org/djumanoff/id_dj/lib"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"time"
	"math/rand"
)

type userPwdResetRepo struct {
	db *sql.DB
}

func NewPwdResetRepo(db *sql.DB) lib.UserPwdResetRepository {
	return &userPwdResetRepo{db:db}
}

func (repo *userPwdResetRepo) Read(userID int64) (string, error) {
	var resetToken string
	err := repo.db.QueryRow("SELECT reset_token FROM user_password_resets WHERE user_id = ?", userID).Scan(&resetToken)

	if err != nil {
		return "", err
	}

	return resetToken, nil
}

func (repo *userPwdResetRepo) Create(data *lib.UserPwdReset) (string, error) {
	token := generatePwdResetToken(8)
	_, err := repo.db.Exec("INSERT INTO user_password_resets (user_id, reset_token, email, created_at, valid_until) VALUES (?, ?, ?, ?, ?)",
		data.UserID, token, data.Email, time.Now(), time.Now().Add(1 * time.Hour))

	if err != nil {
		return "", err
	}

	return token, nil
}

func (repo *userPwdResetRepo) Delete(userID int64) error {
	_, err := repo.db.Exec("DELETE FROM user_password_resets WHERE user_id = ?", userID)
	if err != nil {
		return err
	}
	return nil
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func generatePwdResetToken(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
