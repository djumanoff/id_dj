package cqrs

import "bitbucket.org/djumanoff/id_dj/lib"

type (
	ProtectedProfileCommand interface {
		Exec(api lib.ProtectedProfileAPI) (interface{}, error)
	}

	ProtectedProfileCommandHandler interface {
		ExecCommand(cmd ProtectedProfileCommand) (interface{}, error)
	}
)

type protectedProfileCommandHandler struct {
	profileApi lib.ProtectedProfileAPI
}

func NewProtectedProfileCommandHandler(profileApi lib.ProtectedProfileAPI) ProtectedProfileCommandHandler {
	return &protectedProfileCommandHandler{profileApi}
}

func (ch *protectedProfileCommandHandler) ExecCommand(cmd ProtectedProfileCommand) (interface{}, error) {
	return cmd.Exec(ch.profileApi)
}

type ProtectedGetProfileCommand struct {
	UserID int64
	AccessToken string
}

func (cmd *ProtectedGetProfileCommand) Exec(api lib.ProtectedProfileAPI) (interface{}, error) {
	user, err := api.GetProfile(cmd.UserID, cmd.AccessToken)
	if err != nil {
		return nil, err
	}

	return user, nil
}

type ProtectedUpdateProfileCommand struct {
	UserID int64
	ProfileUpdate *lib.ProfileUpdate
	AccessToken string
}

func (cmd *ProtectedUpdateProfileCommand) Exec(api lib.ProtectedProfileAPI) (interface{}, error) {
	user, err := api.UpdateProfile(cmd.UserID, cmd.ProfileUpdate, cmd.AccessToken)
	if err != nil {
		return nil, err
	}

	return user, nil
}
