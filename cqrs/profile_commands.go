package cqrs

import "bitbucket.org/djumanoff/id_dj/lib"

type (
	ProfileCommand interface {
		Exec(api lib.ProfileAPI) (interface{}, error)
	}

	ProfileCommandHandler interface {
		ExecCommand(cmd ProfileCommand) (interface{}, error)
	}
)

type profileCommandHandler struct {
	profileApi lib.ProfileAPI
}

func NewProfileCommandHandler(profileApi lib.ProfileAPI) ProfileCommandHandler {
	return &profileCommandHandler{profileApi}
}

func (ch *profileCommandHandler) ExecCommand(cmd ProfileCommand) (interface{}, error) {
	return cmd.Exec(ch.profileApi)
}

type GetProfileCommand struct {
	UserID int64
}

func (cmd *GetProfileCommand) Exec(api lib.ProfileAPI) (interface{}, error) {
	user, err := api.GetProfile(cmd.UserID)
	if err != nil {
		return nil, err
	}

	return user, nil
}

type UpdateProfileCommand struct {
	UserID int64
	ProfileUpdate *lib.ProfileUpdate
}

func (cmd *UpdateProfileCommand) Exec(api lib.ProfileAPI) (interface{}, error) {
	user, err := api.UpdateProfile(cmd.UserID, cmd.ProfileUpdate)
	if err != nil {
		return nil, err
	}

	return user, nil
}

