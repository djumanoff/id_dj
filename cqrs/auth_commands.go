package cqrs

import (
	"bitbucket.org/djumanoff/id_dj/lib"
)

type (
	AuthCommand interface {
		Exec(api lib.AuthAPI) (interface{}, error)
	}

	AuthCommandHandler interface {
		ExecCommand(cmd AuthCommand) (interface{}, error)
	}
)

type authCommandHandler struct {
	authApi lib.AuthAPI
}

func NewAuthCommandHandler(authApi lib.AuthAPI) AuthCommandHandler {
	return &authCommandHandler{authApi}
}

func (ch *authCommandHandler) ExecCommand(cmd AuthCommand) (interface{}, error) {
	return cmd.Exec(ch.authApi)
}

type SignUpCommand struct {
	Email string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

type SignUpCommandResult struct {
	UserID int64 `json:"user_id,omitempty"`
	AccessToken string `json:"access_token,omitempty"`
}

func (cmd *SignUpCommand) Exec(api lib.AuthAPI) (interface{}, error) {
	userID, accessToken, err := api.SignUp(cmd.Email, cmd.Password)
	if err != nil {
		return nil, err
	}

	res := &SignUpCommandResult{
		UserID: userID,
		AccessToken: accessToken,
	}

	return res, nil
}


type LoginCommand struct {
	Email string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

type LoginCommandResult struct {
	*lib.User
	AccessToken string `json:"access_token,omitempty"`
}

func (cmd *LoginCommand) Exec(api lib.AuthAPI) (interface{}, error) {
	user, accessToken, err := api.Login(cmd.Email, cmd.Password)
	if err != nil {
		return nil, err
	}

	res := &LoginCommandResult{
		User: user,
		AccessToken: accessToken,
	}

	return res, nil
}


type ResetPasswordCommand struct {
	Email string `json:"email,omitempty"`
}

type ResetPasswordCommandResult struct {
	ResetToken string `json:"reset_token,omitempty"`
}

func (cmd *ResetPasswordCommand) Exec(api lib.AuthAPI) (interface{}, error) {
	resetToken, err := api.ResetPassword(cmd.Email)
	if err != nil {
		return nil, err
	}

	res := &ResetPasswordCommandResult{
		ResetToken: resetToken,
	}

	return res, nil
}


type SetPasswordCommand struct {
	UserID int64 `json:"user_id,omitempty"`
	Password string `json:"password,omitempty"`
	ResetToken string `json:"reset_token,omitempty"`
}

type SetPasswordCommandResult struct {}

func (cmd *SetPasswordCommand) Exec(api lib.AuthAPI) (interface{}, error) {
	err := api.SetPassword(cmd.UserID, cmd.Password, cmd.ResetToken)
	if err != nil {
		return nil, err
	}

	res := &SetPasswordCommandResult{}

	return res, nil
}


type LogoutCommand struct {
	AccessToken string `json:"access_token,omitempty"`
}

type LogoutCommandResult struct {}

func (cmd *LogoutCommand) Exec(api lib.AuthAPI) (interface{}, error) {
	err := api.Logout(cmd.AccessToken)
	if err != nil {
		return nil, err
	}

	res := &LogoutCommandResult{}

	return res, nil
}
