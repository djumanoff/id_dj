package mailer

import (
	"net/smtp"
	"strconv"
)

type (
	Mailer interface {
		SendEmail(to, from, subject, body string) error
	}

	Config struct {
		Host string
		Port int
		Username string
		Password string
	}

	mailer struct {
		auth smtp.Auth
		endpoint string
	}
)

func New(cfg Config) Mailer {
	auth := smtp.PlainAuth("", cfg.Username, cfg.Password, cfg.Host)
	endpoint := cfg.Host + ":" + strconv.Itoa(cfg.Port)

	return &mailer{auth, endpoint}
}

func (m *mailer) SendEmail(to, from, subject, body string) error {
	return smtp.SendMail(m.endpoint, m.auth, from, []string{to}, []byte(body))
}
