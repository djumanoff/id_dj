package logger

import (
	log "github.com/Sirupsen/logrus"
)

type Logger interface {
	Debug(args ...interface{})

	Info(args ...interface{})

	Warn(args ...interface{})

	Error(args ...interface{})

	Fatal(args ...interface{})

	Panic(args ...interface{})
}

func New(module string) Logger {
	return log.WithFields(log.Fields{
		"module": module,
	})
}
