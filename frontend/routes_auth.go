package frontend

import (
	"gopkg.in/kataras/iris.v6"
	"bitbucket.org/djumanoff/id_dj/cqrs"
)

type HttpAuthEndpointFactory struct {
	ch cqrs.AuthCommandHandler
}

func NewHttpAuthEndpointFactory(ch cqrs.AuthCommandHandler) *HttpAuthEndpointFactory {
	return &HttpAuthEndpointFactory{ch}
}

type SetPasswordData struct {
	UserID int64
	Password string
	ConfirmPassword string
	ResetToken string
}

type SignUpData struct {
	Email string
	Password string
	ConfirmPassword string
}

type LoginData struct {
	Email string
	Password string
}

type ResetPasswordData struct {
	Email string
}

func (fac *HttpAuthEndpointFactory) MakeGetError(tmpl, message string) iris.HandlerFunc {
	return func(ctx *iris.Context) {
		ctx.Render( "error.html", iris.Map{"Title": tmpl, "Message": message})
	}
}

func (fac *HttpAuthEndpointFactory) MakeGetLogin() iris.HandlerFunc {
	return func(ctx *iris.Context) {
		ctx.Render("login.html", iris.Map{"Title": "Login"})
	}
}

func (fac *HttpAuthEndpointFactory) MakePostLogin() iris.HandlerFunc {
	return func(ctx *iris.Context) {
		loginData := &LoginData{}
		err := ctx.ReadForm(loginData)
		if err != nil {
			ctx.Render("login.html", iris.Map{"Error": err.Error()})
			return
		}

		cmd := &cqrs.LoginCommand{
			Email: loginData.Email,
			Password: loginData.Password,
		}
		result, err := fac.ch.ExecCommand(cmd)
		if err != nil {
			ctx.Render("login.html", iris.Map{"Error": err.Error()})
			return
		}
		data := result.(*cqrs.LoginCommandResult)

		ctx.Session().Set("access_token", data.AccessToken)
		ctx.Session().Set("user_id", data.UserID)

		ctx.Redirect("/users/me")
	}
}

func (fac *HttpAuthEndpointFactory) MakeGetResetPassword() iris.HandlerFunc {
	return func(ctx *iris.Context) {
		ctx.Render("reset_password.html", iris.Map{"Title": "Reset Password"})
	}
}

func (fac *HttpAuthEndpointFactory) MakeGetResetPasswordResult() iris.HandlerFunc {
	return func(ctx *iris.Context) {
		ctx.Render("reset_password_result.html", iris.Map{"Title": "Reset Password Result"})
	}
}

func (fac *HttpAuthEndpointFactory) MakePostResetPassword() iris.HandlerFunc {
	return func(ctx *iris.Context) {
		formData := &ResetPasswordData{}
		err := ctx.ReadForm(formData)
		if err != nil {
			ctx.Render("reset_password.html", iris.Map{"Error": err.Error()})
			return
		}

		cmd := &cqrs.ResetPasswordCommand{
			Email: formData.Email,
		}
		_, err = fac.ch.ExecCommand(cmd)
		if err != nil {
			ctx.Render("reset_password.html", iris.Map{"Error": err.Error()})
			return
		}

		ctx.Redirect("/password/reset/result")
	}
}

func (fac *HttpAuthEndpointFactory) MakeGetSetPassword() iris.HandlerFunc {
	return func(ctx *iris.Context) {
		resetToken := ctx.Request.URL.Query().Get("token")
		userIDstr := ctx.Request.URL.Query().Get("id")

		ctx.Render("set_password.html", iris.Map{"Title": "Reset Password", "ResetToken": resetToken, "UserID": userIDstr})
	}
}

func (fac *HttpAuthEndpointFactory) MakePostSetPassword() iris.HandlerFunc {
	return func(ctx *iris.Context) {
		formData := &SetPasswordData{}
		err := ctx.ReadForm(formData)
		if err != nil {
			ctx.Render("set_password.html", iris.Map{
				"Title": "Reset Password",
				"ResetToken": formData.ResetToken,
				"UserID": formData.UserID,
				"Error": err.Error(),
			})
			return
		}

		if formData.Password != formData.ConfirmPassword {
			ctx.Render("set_password.html", iris.Map{
				"Title": "Reset Password",
				"ResetToken": formData.ResetToken,
				"UserID": formData.UserID,
				"Error": "Password and confirm password don't match",
			})
			return
		}

		cmd := &cqrs.SetPasswordCommand{
			UserID: formData.UserID,
			Password: formData.Password,
			ResetToken: formData.ResetToken,
		}
		_, err = fac.ch.ExecCommand(cmd)
		if err != nil {
			ctx.Render("set_password.html", iris.Map{
				"Title": "Reset Password",
				"ResetToken": formData.ResetToken,
				"UserID": formData.UserID,
				"Error": err.Error(),
			})
			return
		}

		ctx.Redirect("/login")
	}
}

func (fac *HttpAuthEndpointFactory) MakeGetSignUp() iris.HandlerFunc {
	return func(ctx *iris.Context) {
		ctx.Render("signup.html", iris.Map{"Title": "Sign Up"})
	}
}

func (fac *HttpAuthEndpointFactory) MakePostSignUp() iris.HandlerFunc {
	return func(ctx *iris.Context) {
		signupData := &SignUpData{}
		err := ctx.ReadForm(signupData)
		if err != nil {
			ctx.Render("signup.html", iris.Map{"Error": err.Error()})
			return
		}

		if signupData.Password != signupData.ConfirmPassword {
			ctx.Render("signup.html", iris.Map{"Error": "Password and confirm password don't match"})
			return
		}

		cmd := &cqrs.SignUpCommand{
			Email: signupData.Email,
			Password: signupData.Password,
		}
		result, err := fac.ch.ExecCommand(cmd)
		if err != nil {
			ctx.Render("signup.html", iris.Map{"Error": err.Error()})
			return
		}
		data := result.(*cqrs.SignUpCommandResult)

		ctx.Session().Set("access_token", data.AccessToken)
		ctx.Session().Set("user_id", data.UserID)

		ctx.Redirect("/users/me/edit")
	}
}

func (fac *HttpAuthEndpointFactory) MakeLogout() iris.HandlerFunc {
	return func(ctx * iris.Context) {
		ctx.Session().Delete("access_token")
		ctx.Session().Delete("user_id")

		ctx.Redirect("/login")
	}
}
