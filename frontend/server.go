package frontend

import (
	"bitbucket.org/djumanoff/id_dj/http/api"
	"bitbucket.org/djumanoff/id_dj/cqrs"
)

type Config struct {
	Endpoint string
	APIBaseUrl string
}

func Run(cfg Config) error {
	app := initApp()

	authApi, err := api.NewAuthAPI(cfg.APIBaseUrl)
	if err != nil {
		return err
	}

	profileApi, err := api.NewProfileAPI(cfg.APIBaseUrl)
	if err != nil {
		return err
	}

	authCmdHandler := cqrs.NewAuthCommandHandler(authApi)
	profileCmdHandler := cqrs.NewProtectedProfileCommandHandler(profileApi)

	authFac := NewHttpAuthEndpointFactory(authCmdHandler)
	profileFac := NewHttpProfileEndpointFactory(profileCmdHandler)

	initRoutes(app, authFac, profileFac)

	app.Listen(cfg.Endpoint)

	return nil
}
