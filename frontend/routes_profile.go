package frontend

import (
	"bitbucket.org/djumanoff/id_dj/cqrs"
	"gopkg.in/kataras/iris.v6"
	"bitbucket.org/djumanoff/id_dj/lib"
)

type HttpProfileEndpointFactory struct {
	ch cqrs.ProtectedProfileCommandHandler
}

func NewHttpProfileEndpointFactory(ch cqrs.ProtectedProfileCommandHandler) *HttpProfileEndpointFactory {
	return &HttpProfileEndpointFactory{ch}
}

func (fac *HttpProfileEndpointFactory) MakeGetProfile() iris.HandlerFunc {
	return func(ctx *iris.Context) {
		userID, ok := ctx.Session().Get("user_id").(int64)
		if !ok {
			ctx.Redirect("/logout")
			return
		}
		accessToken := ctx.Session().Get("access_token").(string)
		cmd := &cqrs.ProtectedGetProfileCommand{userID, accessToken}
		result, err := fac.ch.ExecCommand(cmd)
		if err != nil {
			ctx.Redirect("/error")
			return
		}
		data, ok := result.(*lib.User)
		if !ok {
			ctx.Redirect("/error")
			return
		}

		ctx.Render("profile.html", iris.Map{"Title": "Profile", "User": data})
	}
}

func (fac *HttpProfileEndpointFactory) MakeGetEditProfile() iris.HandlerFunc {
	return func(ctx *iris.Context) {
		userID, ok := ctx.Session().Get("user_id").(int64)
		if !ok {
			ctx.Redirect("/logout")
			return
		}
		accessToken := ctx.Session().Get("access_token").(string)
		cmd := &cqrs.ProtectedGetProfileCommand{userID, accessToken}
		result, err := fac.ch.ExecCommand(cmd)
		if err != nil {
			ctx.Redirect("/error")
			return
		}
		data, ok := result.(*lib.User)
		if !ok {
			ctx.Redirect("/error")
			return
		}

		ctx.Render("edit_profile.html", iris.Map{"Title": "Profile", "User": data})
	}
}

func (fac *HttpProfileEndpointFactory) MakePostEditProfile() iris.HandlerFunc {
	return func(ctx *iris.Context) {
		userID, ok := ctx.Session().Get("user_id").(int64)
		if !ok {
			ctx.Redirect("/error")
			return
		}

		profileData := &lib.ProfileUpdate{}
		err := ctx.ReadForm(profileData)
		if err != nil {
			ctx.Redirect("/error")
			return
		}

		accessToken := ctx.Session().Get("access_token").(string)
		cmd := &cqrs.ProtectedUpdateProfileCommand{userID, profileData, accessToken}
		_, err = fac.ch.ExecCommand(cmd)
		if err != nil {
			ctx.Redirect("/error")
			return
		}

		ctx.Redirect("/users/me")
	}
}
