package frontend

import (
	"gopkg.in/kataras/iris.v6"
)

func initRoutes(app *iris.Framework, authFac *HttpAuthEndpointFactory, profileFac *HttpProfileEndpointFactory) {
	app.OnError(iris.StatusNotFound, authFac.MakeGetError("404", "Page can not be found."))

	app.Get("/error", authFac.MakeGetError("500", "Internal error."))

	app.Get("/", authFac.MakeGetSignUp())

	app.Get("/signup", authFac.MakeGetSignUp())

	app.Post("/signup", authFac.MakePostSignUp())

	app.Get("/login", authFac.MakeGetLogin())

	app.Post("/login", authFac.MakePostLogin())

	app.Get("/logout", authFac.MakeLogout())

	app.Get("/password/set", authFac.MakeGetSetPassword())

	app.Post("/password/set", authFac.MakePostSetPassword())

	app.Get("/password/reset", authFac.MakeGetResetPassword())

	app.Get("/password/reset/result", authFac.MakeGetResetPasswordResult())

	app.Post("/password/reset", authFac.MakePostResetPassword())

	app.Get("/users/me", profileFac.MakeGetProfile())

	app.Get("/users/me/edit", profileFac.MakeGetEditProfile())

	app.Post("/users/me/edit", profileFac.MakePostEditProfile())
}
